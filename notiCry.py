# -*- coding: utf-8 -*-

#Andrew Kaplan, 3/26/15, Social Software Project 'notiCRY'
#Twilio Code Borrowed from Julia Grace. @jewelia THANK YOU!

'''
IMPORTANT:
Check README file if there are issues running this program
'''
import settings
import sys

from BreakfastSerial import Led, Arduino, Button
from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

#instantiate variables
board = Arduino()
LED_PIN = 13
BUTTON_PIN = 2

led = Led(board, LED_PIN)
button = Button(board, BUTTON_PIN)

msg_sent = False
count = 0
textLim = 5 #this number is the max number of messages you wish to have sent


#------------------------------SEND MESSAGE CODE BELOW-------------------------------------

def down_press():
  global msg_sent
  global count
  print "Circuit Closed!"

  if not msg_sent: # if msg_sent is any kind of 0 or empty container, or false...execute code

      # Turn on the LED to indicate sending the txt message!
      led.on()
      try:
          client = TwilioRestClient(settings.twilio_account_sid,
                                settings.twilio_auth_token)
          message = client.sms.messages.create(
              body="UH-OH! It looks like Norma is in distress! YOU CAN HELP!",
              to=settings.your_phone_number,
              from_=settings.your_twilio_number)

      except TwilioRestException as e:
          print "Ruh-roh got an error: %s" % e
          led.off()
          sys.exit(0)

      print "Attempting to send message, status is: %s" % message.status
      #check here for more REST API info for getting info about text ^^^
      #https://www.twilio.com/docs/api/rest/message#sms-status-values
      msg_sent = True
      count += 1 #count/limit how many messages have been sent
      led.off()
      contRun(textLim)
  else:
    contRun(textLim)


def up_press():
  print "Circuit Open!"

 #---------------------------CONTINUOUS RUN (3/26/15,working) ---------------------------------- 

def contRun(textLim): #to limit number of times program runs, or to keep it continuing to run
  global msg_sent

  if(count < textLim):
    msg_sent = False
    print count, "Message(s) Sent" #to let you know how many messages have been sent to device
  else:
    print "I HOPE YOUR FRIEND HAS ARRIVED! Your message limit of", textLim, "messages has been reached"
    
def start():
  if(count == 0):#if count is 0 wait for tears
    print "Waiting for Tears!"
    button.down(down_press)
    button.up(up_press) 

start()


#----------------------CALL FUNCTIONS (without continuous run)----------------------------------

# button.down(down_press) #execute 'closed' function when circuit is closed
# button.up(up_press) #excecute 'open' function when circuit is open
# print "Ready To Send Message!" # Message to let you know that code is ready to run
# print count, "Messages Sent"
# while(not msg_sent): # while msg_sent is false, skip next iteration
#     continue
















